package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

    // 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String NOT_EXIST = "ファイルが存在しません";
	private static final String INVALID_FORMAT = "ファイルのフォーマットが不正です";
	private static final String SHOP_FILE = "支店定義";
	private static final String GOODS_FILE = "商品定義";
	private static final String FILE_NOT_NUMBER = "売上ファイル名が連番になっていません";
	private static final String NOT_FORMAT = "のフォーマットが不正です";
	private static final String NOT_SHOP_CODE = "の支店コードが不正です";
	private static final String NOT_GOODS_CODE = "の商品コードが不正です";
	private static final String SUM_UPPER_LIMIT = "合計金額が10桁を超えました";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数の確認
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと商品金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", SHOP_FILE)) {
			return;
		}
        //readfileを使って商品定義ファイルを読み込む処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[A-Za-z0-9]{8}$", GOODS_FILE)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		// ファイルが8桁+「.rcb」の場合rcbファイルに追加していく+rcdファイルがファイルなのかディレクトリなのか確認
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		// 売上ファイル名が連番になっているかの確認処理
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println(FILE_NOT_NUMBER);
				return;
			}
		}
		// 売上ファイル読み込み。
		BufferedReader br = null;

		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;
				// 箱を作る
				List<String> linelist = new ArrayList<>();
				// 一行一行読み込んでいく処理
				while ((line = br.readLine()) != null) {
					linelist.add(line);
				}
				// 売上ファイルの中身が4桁以上ある場合
				if(linelist.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + NOT_FORMAT);
					return;
				}
				// 売上ファイルの支店コードが支店定義ファイルに存在するか
				if(!branchSales.containsKey(linelist.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + NOT_SHOP_CODE);
					return;
				}
				// 売上ファイルの商品コードが商品定義ファイルに存在するか
				if(!commodityNames.containsKey(linelist.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + NOT_GOODS_CODE);
					return;
				}
				// 売上ファイルが数字なのか確認
				if(!linelist.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				// 支店ファイルの計算
				long fileSale = Long.parseLong(linelist.get(2));
				long sum = branchSales.get(linelist.get(0)) + fileSale;
				// 売上金額が11桁以上の場合
				if (sum >= 10000000000L) {
					System.out.println(SUM_UPPER_LIMIT);
					return;
				}
				branchSales.put(linelist.get(0), sum);
				// 商品ファイルの計算
				long goodsSale = Long.parseLong(linelist.get(2));
				long goodssum = commoditySales.get(linelist.get(1)) + goodsSale;
				// 売上金額が11桁以上の場合
				if (goodssum >= 10000000000L) {
					System.out.println(SUM_UPPER_LIMIT);
					return;
				}
				commoditySales.put(linelist.get(1), goodssum);
			}catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales, String rgx, String sgFile) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			// もし支店定義ファイルが存在しなかったら
			if(!file.exists()) {
				System.out.println(sgFile + NOT_EXIST);
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");
				//「,」区切りされていてかつ支店コードは数字３桁ではない場合
				if((items.length != 2) || (!items[0].matches(rgx))) {
					System.out.println(sgFile + INVALID_FORMAT);
				}
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], (long)0);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter br = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			// キーから支店名、売上集計を書き出していく
			for(String key : sales.keySet()){
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
		} finally {
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
